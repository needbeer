#                                 ___
#      ___   __                  | | |
#      |  \  | | ____  ____   __ | | | __   ____  ____  _  __
#      |   \ | |/ /_\\/ /_\\ /   \ | /   \ / /_\\/ /_\\| |//
#      | |\ \| |  \___  \___(  o   |   o  )  \___  \___|  /
#      |_| \___|\____/\____/ \____/ \____/ \____/\____/|_|
#
#
#      (c) 2010 Thomas Martitz
#
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#      
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#      
#      You should have received a copy of the GNU General Public License
#      along with this program; if not, write to the Free Software
#      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#      MA 02110-1301, USA.
#

top = '.'
out = 'build'


APPNAME = 'needbeer'
VERSION = '0.0.1'


beer_sources = [
	'src/main.c',
    'src/backend.c',
    'src/gui.c',
    'src/backends/mpd/mpd_init.c' ]

def configure(conf):               
    import Configure
    have_c99 = True
    print('→ configuring the project')
    conf.check_tool('compiler_cc')
    conf.check_cfg(package='libmpdclient', atleast_version='2.0', uselib_store='MPD',
		mandatory=True, args='--cflags --libs')
    conf.check_cfg(package='gtk+-2.0', atleast_version='2.8.0', uselib_store='GTK',
		mandatory=True, args='--cflags --libs')
    conf.env.append_value('CCFLAGS', '-g -O2'.split())
    conf.define('VERSION', VERSION, 1)


def build(ctx):
    print('building the software')
    ctx.new_task_gen(
		features		= 'cc cprogram',
		name			= 'main',
		target			= 'main',
		source			= beer_sources,
		includes		= 'src/ src/backends/mpd/',
		uselib			= 'MPD GTK',
        dict			= { 'VERSION' : VERSION },
	)
def set_options(opt):
	opt.tool_options('compiler_cc')
