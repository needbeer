/*                                 ___
 *      ___   __                  | | |
 *      |  \  | | ____  ____   __ | | | __   ____  ____  _  __
 *      |   \ | |/ /_\\/ /_\\ /   \ | /   \ / /_\\/ /_\\| |//
 *      | |\ \| |  \___  \___(  o   |   o  )  \___  \___|  /
 *      |_| \___|\____/\____/ \____/ \____/ \____/\____/|_|
 *
 *
 *      (c) 2010 Thomas Martitz
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include <stdio.h>
#include <stdbool.h>
#include <gtk/gtk.h>

#include "backend.h"


/* based on GTK+ Hello World example ( http://library.gnome.org/devel/gtk-tutorial/2.17/c39.html#SEC-HELLOWORLD ) */


/* This is a callback function. The data arguments are ignored
 * in this example. More on callbacks below. */
static void next_clicked( GtkWidget *widget,
                   gpointer   data )
{ 
    beer_backend_next();
}

static void prev_clicked( GtkWidget *widget,
                   gpointer   data )
{ 
    beer_backend_prev();
}

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

    g_print ("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return FALSE;
}

/* Another callback */
static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

int gui_init(int *argc, char***argv)
{
    /* This is called in all GTK applications. Arguments are parsed
     * from the command line and are returned to the application. */
    return !gtk_init_check(argc, argv);
}

int gui_show(void)
{
    /* GtkWidget is the storage type for widgets */
    GtkWidget *window;
    GtkWidget *button_prev;
    GtkWidget *button_next;
    GtkWidget *hbox;
    
    /* create a new window */
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    
    /* When the window is given the "delete_event" signal (this is given
     * by the window manager, usually by the "close" option, or on the
     * titlebar), we ask it to call the delete_event () function
     * as defined above. The data passed to the callback
     * function is NULL and is ignored in the callback function. */
    g_signal_connect (G_OBJECT (window), "delete_event",
		      G_CALLBACK (delete_event), NULL);
    
    /* Here we connect the "destroy" event to a signal handler.  
     * This event occurs when we call gtk_widget_destroy() on the window,
     * or if we return FALSE in the "delete_event" callback. */
    g_signal_connect (G_OBJECT (window), "destroy",
		      G_CALLBACK (destroy), NULL);
    
    /* Sets the border width of the window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);

    hbox = gtk_hbox_new(true, 3);
    /* Creates a new button with the label "Hello World". */
    button_next = gtk_button_new_with_label ("_Next");
    button_prev = gtk_button_new_with_label ("_Previos");
    
    /* When the button receives the "clicked" signal, it will call the
     * function hello() passing it NULL as its argument.  The hello()
     * function is defined above. */
    g_signal_connect (G_OBJECT (button_next), "clicked",
		      G_CALLBACK (next_clicked), NULL);
    g_signal_connect (G_OBJECT (button_prev), "clicked",
		      G_CALLBACK (prev_clicked), NULL);
    
    /* This will cause the window to be destroyed by calling
     * gtk_widget_destroy(window) when "clicked".  Again, the destroy
     * signal could come from here, or the window manager. */
/*
    g_signal_connect_swapped (G_OBJECT (button), "clicked",
			      G_CALLBACK (gtk_widget_destroy),
                              G_OBJECT (window));
*/
    
    /* This packs the button into the window (a gtk container). */
    gtk_box_pack_start_defaults(GTK_BOX(hbox), button_next);
    gtk_box_pack_start_defaults(GTK_BOX(hbox), button_prev);
    gtk_container_add (GTK_CONTAINER (window), hbox);
    
    /* The final step is to display this newly created widget. */
    gtk_widget_show(button_next);
    gtk_widget_show(button_prev);
    gtk_widget_show(hbox);
    
    /* and the window */
    gtk_widget_show(window);
    
    /* All GTK applications must have a gtk_main(). Control ends here
     * and waits for an event to occur (like a key press or
     * mouse event). */
    gtk_main ();
    
    return 0;
}

