/*                                 ___
 *      ___   __                  | | |
 *      |  \  | | ____  ____   __ | | | __   ____  ____  _  __
 *      |   \ | |/ /_\\/ /_\\ /   \ | /   \ / /_\\/ /_\\| |//
 *      | |\ \| |  \___  \___(  o   |   o  )  \___  \___|  /
 *      |_| \___|\____/\____/ \____/ \____/ \____/\____/|_|
 *
 *
 *      (c) 2010 Thomas Martitz
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>
    
#include "backend.h"
#include "gui.h"


#define VERSION "0.0.1"


void show_help(void)
{
    printf("blah\n");
    exit(0);
}

void show_version(void)
{
    printf("Need Beer\nVersion: %s\n", VERSION);
    exit(0);
}

#define NEXT 1<<0
#define PREV 1<<1

int parse_argv(int argc, char** argv)
{
    int ret = 0;
    if (argc > 1)
    {
        int i;
        for(i = 1; i < argc; i++)
        {
            char* this_arg = argv[i];
            bool single_mode = (this_arg[0] == '-' && this_arg[1] != '-');
            if (single_mode)
            {
                int j;
                char ch;
                for(j = 1; ch = this_arg[j], isalnum(ch); j++)
                {
                    switch(ch) {
                        case 'h':   show_help();    break;
                        case 'v':   show_version(); break;
                        case 'n':   ret |= NEXT;    break;
                        case 'p':   ret |= PREV;    break;
                        default:                    break;
                    }
                }
            }
            else
            {
                if (!strcasecmp(this_arg+2, "help"))
                    show_help();
                else if (!strcasecmp(this_arg+2, "version"))
                    show_version();
                else if (!strcasecmp(this_arg+2, "prev"))
                    ret |= PREV;
                else if (!strcasecmp(this_arg+2, "next"))
                    ret |= NEXT;
            }
        }
    }
    return ret;
}

void sigterm(int signal)
{   
    beer_backend_cleanup();
}

int main(int argc, char *argv[])
{
    int rc;
    int args = parse_argv(argc, argv);
    rc = beer_backend_init();
    signal(SIGTERM, sigterm); /* be sure to deinitialize on exit */

    if (rc)
    {
        printf("backend failed to initialize\n");
        return 1;
    }
    rc = gui_init(&argc, &argv);
    if (rc)
    {
        printf("gui failed to initialize");
        beer_backend_cleanup();
        return 2;
    }

    if (args & NEXT)
        beer_backend_next();
    if (args & PREV)
        beer_backend_prev();

    gui_show();
    beer_backend_cleanup();
    return 0;
}
