/*                                 ___
 *      ___   __                  | | |
 *      |  \  | | ____  ____   __ | | | __   ____  ____  _  __
 *      |   \ | |/ /_\\/ /_\\ /   \ | /   \ / /_\\/ /_\\| |//
 *      | |\ \| |  \___  \___(  o   |   o  )  \___  \___|  /
 *      |_| \___|\____/\____/ \____/ \____/ \____/\____/|_|
 *
 *
 *      (c) 2010 Thomas Martitz
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <stdio.h>


#include "backend.h"
#include <mpd/client.h>
#include <mpd/error.h>
#include <mpd/connection.h>


static struct mpd_connection *mpd = NULL;

static int mpd_backend_cleanup(void)
{
    mpd_connection_free(mpd);
    return 0;
}

static int mpd_backend_init(void)
{
    enum mpd_error err;

    mpd = mpd_connection_new("localhost", 6600, 2*1000*1000);

    if (!mpd)
        return 1;

    err = mpd_connection_get_error(mpd);
    if (err != MPD_ERROR_SUCCESS)
    {
        printf("err: %s\n", mpd_connection_get_error_message(mpd));
        return 2;
    }
    return 0;
}

static int mpd_backend_next(void)
{
    mpd_run_next(mpd);
    return 0;
}

static int mpd_backend_prev(void)
{
    mpd_run_previous(mpd);
    return 0;
}

struct beer_backend beer_mpd_backend = 
{
    .init = mpd_backend_init,
    .cleanup = mpd_backend_cleanup,
    .next = mpd_backend_next,
    .prev = mpd_backend_prev,
};
